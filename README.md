# Starbase Autotank - 2 Tanksystem

Für interessierte stelle ich hier mal mein Script ein welches die Propellant Tanks paarweise leert. 
Dadurch kann man unterwegs leergelaufene Tanks nachfüllen ohne eine Taschentanke dabei haben zu müssen.

Marmot - 2 Tank System
Wichtiger Schritt vorher:
Als erster und wichtigster Schritt muss erstmal festgestellt werden ob die Propellant-Tanks losgelöst werden können ohne Kabel/Rohre abzureissen.
hierzu peilt man mit dem Bolt tool den Tank an (NICHT DIE HALTERUNGEN!!!) und löst per Rechtsklick sämtliche Bolzen aus dem Tank.
Sollte der Tank nun lose sein bekommt er eine dünne Blaue Umrandung. Jetzt sollte mit der P Taste der Tank eingesammelt werden können. Sollte dies nicht der 
Fall sein und es wird angezeigt dass ein Kabel oder Rohr an dem Tank "klebt" sollte keinesfalls versucht werden den Tank herauszureissen! 
In dem Fall kann dieses AutoTank system leider nicht wirklich genutzt werden.
-------------------------------------------------------------------------------------------------------
Aufbau:
Benötigt werden:
1 Standart Yolol Chip
3 Buttons nach Wahl. im Beispiel 2x 12x12 Button und 1x 12x24 Button
2 Fortschrittsanzeigen/Displays die die Tankfüllen Visualisieren


die Bauteile können nach Belieben Montiert werden, oder in der Anordnung wie im Beispiel.
Die Felder müssen wie Folgt geändert werden.

1. Button (12x12)
ButtonState zu IsOpen1
ButtonColor zu PropColor1

2. Button (12x12)
ButtonState zu IsOpen2
ButtonColor zu PropColor2

3. Button (12x24)
ButtonState zu AutoTank

1. Display
PanelValue zu Propellant1
PanelMaxvalue bekommt den Wert des Maximalen Tankinhaltes eingetragen. (Medium Tank zb. 4000000)

2. Display
PanelValue zu Propellant2
PanelMaxvalue bekommt den Wert des Maximalen Tankinhaltes eingetragen. (Medium Tank zb. 4000000)
--------------------------------------------------------------------------------------------------------
Die beiden Propellant Tanks werden dann wie folgt abgeändert.
1. Tank: GasContainerStoredResource zu Propellant1
1. Tank Befestigungen: IsopenID zu IsOpen1

2. Tank: GasContainerStoredResource zu Propellant2
2. Tank Befestigungen: IsopenID zu IsOpen2
---------------------------------------------------------------------------------------------------------
Yolol:
in einen Standart Yolol Chip kommt der folgende Code rein:
[klickmich](https://gitlab.com/holgerheinemann6/starbase-autotank-2-tanksystem/-/blob/main/Autotank.yolol)

if :propellant2 <800000 then :PropColor2 = 1 else :PropColor2 =2 end
if :propellant2 <800000 then :PropColor2 = 1 else :PropColor2 =2 end
if :autotank==1 then goto4 end goto1
if :propellant1 <=0.3 then goto6 end
if :propellant2 <=0.3 or :propellant1 <:propellant2 then goto8 end
if :propellant2 <=0.3 then goto4 end
if :propellant1 <=0.3 or :propellant2<=:propellant1 then goto9  end
:isOpen1=1 :isOpen2=0 goto1
:isOpen1=0 :isOpen2=1 goto1
